import React from 'react';
import { Navbar, Button } from 'react-bootstrap';

const Header = ({ handleToggle }) => {
  return (
    <Navbar bg='dark' variant='dark'>
      <Navbar.Brand href='#home'>React Bootstrap</Navbar.Brand>

      <Navbar.Collapse className='justify-content-end'>
        <Button onClick={() => handleToggle()}>Agregar tarea</Button>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Header;
