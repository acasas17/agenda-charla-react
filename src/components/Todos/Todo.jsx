import React from 'react';
import { ListGroupItem, Button } from 'react-bootstrap';
import { FaEdit, FaTrash } from 'react-icons/fa';

const Todo = ({ todo, handleEdit, handleDelete }) => {
  return (
    <ListGroupItem>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <p>{todo.title}</p>
        <div>
          <Button onClick={() => handleDelete(todo.id)} className='m-2'>
            <FaTrash />
          </Button>
          <Button onClick={() => handleEdit(todo)}>
            <FaEdit />
          </Button>
        </div>
      </div>
    </ListGroupItem>
  );
};

export default Todo;
