import React from 'react';
import { ListGroup, Alert } from 'react-bootstrap';
import Todo from './Todo';

const Todos = (props) => {
  if (props.todos.length === 0 || props.loading) {
    return (
      <Alert variant='warning'>
        {props.loading ? 'Cargando...' : 'No hay ninguna tarea'}
      </Alert>
    );
  }

  return (
    <div>
      <ListGroup>
        {props.todos.map((todo) => {
          return (
            <Todo
              key={todo.id}
              todo={todo}
              handleEdit={props.handleEdit}
              handleDelete={props.handleDelete}
            />
          );
        })}
      </ListGroup>
    </div>
  );
};

export default Todos;
