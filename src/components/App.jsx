import React, { useState, useEffect } from 'react';
import Header from './Header';
import Todos from './Todos/Todos';
import Modal from './Modal';
import axios from 'axios';

const App = () => {
  const [todos, setTodos] = useState([]);
  const [modal, setModal] = useState(false);
  const [loading, setLoading] = useState(true);
  const [actual, setActual] = useState(null);

  useEffect(() => {
    getData();
  }, []);

  function handleToggle() {
    setModal((prev) => !prev);
  }

  async function getData() {
    try {
      const response = await axios.get(
        'https://jsonplaceholder.typicode.com/todos?_limit=10'
      );
      setTodos(response.data);
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  }

  async function handleSubmit(todo) {
    try {
      setLoading(true);

      const response = await axios.post(
        'https://jsonplaceholder.typicode.com/todos',
        todo
      );

      setTodos((prev) => {
        return [response.data, ...prev];
      });
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  }

  async function handleDelete(todo_id) {
    try {
      setLoading(true);

      await axios.delete(
        `https://jsonplaceholder.typicode.com/todos/${todo_id}`
      );

      setTodos((prev) => {
        return prev.filter((each) => each.id !== todo_id);
      });

      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  }

  async function editTodo(todo) {
    try {
      setLoading(true);

      const response = await axios.patch(
        `https://jsonplaceholder.typicode.com/todos/${todo.id}`,
        todo
      );

      setTodos((prev) => {
        return prev.map((each) => {
          if (each.id === todo.id) {
            return {
              ...response.data,
            };
          } else return each;
        });
      });

      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  }

  async function handleEdit(todo) {
    setActual(todo);
    setModal(true);
  }

  return (
    <div>
      <Header handleToggle={handleToggle} />
      <Todos
        todos={todos}
        loading={loading}
        handleEdit={handleEdit}
        handleDelete={handleDelete}
      />
      {modal && (
        <Modal
          show={modal}
          handleToggle={handleToggle}
          handleSubmit={handleSubmit}
          editTodo={editTodo}
          actual={actual}
        />
      )}
    </div>
  );
};

export default App;
