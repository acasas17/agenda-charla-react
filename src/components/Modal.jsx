import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

const ModalComponent = ({
  show,
  handleToggle,
  handleSubmit,
  actual,
  editTodo,
}) => {
  const [title, setTitle] = useState(actual ? actual.title : '');
  const [completed, setCompleted] = useState(actual ? actual.completed : false);

  function onSubmit(e) {
    e.preventDefault();

    const todo = {
      title,
      completed,
      id: actual ? actual.id : null,
    };

    if (actual) {
      editTodo(todo);
    } else {
      handleSubmit(todo);
    }

    handleToggle();
  }

  return (
    <Modal show={show} onHide={handleToggle} centered>
      <Modal.Header closeButton>
        <Modal.Title>
          {actual ? 'Editar una tarea' : 'Agregar una tarea'}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={onSubmit}>
          <Form.Group className='mb-3'>
            <Form.Label>Titulo</Form.Label>
            <Form.Control
              type='text'
              placeholder='Ingresa un titulo'
              value={title}
              onChange={(e) => {
                setTitle(e.target.value);
              }}
            />
          </Form.Group>

          <Form.Group className='mb-3'>
            <Form.Check
              type='checkbox'
              label='Completado'
              checked={completed}
              onChange={(e) => setCompleted(e.target.checked)}
            />
          </Form.Group>
          <Button variant='primary' type='submit'>
            Guardar
          </Button>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant='primary' onClick={handleToggle}>
          Cancelar
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ModalComponent;
